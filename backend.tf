terraform {
  backend "s3" {
    bucket = "backendbucket23"
    key    = "awsdemo.tfstate"
    region = "us-west-2"
    profile = "default"
    dynamodb_table = "tf-state file"
  }
}