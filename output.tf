output "public-ip" {
  value = aws_instance.jjtechweb.public_ip
}

output "private-ip" {
  value = aws_instance.jjtechweb.private_ip
}

